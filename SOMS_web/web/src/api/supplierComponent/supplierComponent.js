import request from '@/utils/request'


export default{
    // 列表查询分页
    // current当前页
    // size每页记录数
    getsupplierComponentList(current,size){
        return request({
            url: `/tbl-supplier-component/pageSupplierComponent/${current}/${size}`,
            method: 'get'
          })
    },
  // 根据id查询供应给供应零件信息
    getSupplierComponentrById(id){
      return request({
        url: `/tbl-supplier-component/querySupplierComponentById/${id}`,
        method: 'get'
      })
    },
    getOptions(id){
      return request({
          url: `/tbl-supplier-component/getOptions/${id}`,
          method: 'get'
        })
  },
     // 删除供应商供应零件信息
     deleteSupplierComponentById(id){
        return request({
            url: `/tbl-supplier-component/${id}`,
            method: 'delete'
          })
      },
      // 添加供应商零件信息
     updateSupplierComponent(supplierComponent){
        return request({
            url: `/tbl-supplier-component/updateSupplierComponent`,
            method: 'post',
            data: supplierComponent
          })
      }
}

