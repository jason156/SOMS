// pages/refer/refer.js

Page({
add:function(event) {
  console.log("add");
  let index=event.currentTarget.dataset.index;
  let order=this.data.order;
  let records=this.data.records;
  let count=this.data.count;
  if(count[index]==null||count[index]==0){
    count[index]=1; 
    let item={};
    item.id=records[index].id;
    item.name=records[index].name;
    item.count=1;
    order[order.length]=item;
  }else{
    count[index]=count[index]+1;
    for(let i=0;i<order.length;i++){
      console.log(i);
      if(order[i].id==records[index].id){
        order[i].count=order[i].count+1;
        break;
      }
    }
  }
  this.setData({
    order:order,
    count:count,
  })
},
sub:function(event) {
  console.log("sub");
  let index=event.currentTarget.dataset.index;
  let order=this.data.order;
  let records=this.data.records;
  let count=this.data.count;
  if(count[index]==0){
    return;
  }
  if(count[index]==1){
    count[index]=0; 
    for(let i=0;i<order.length;i++){
      if(order[i].id==records[index].id){
        for(let j=i;j<order.length-1;j++){
          order[i]=order[i+1];
        }
        order.length--;
        break;
      }
    }
  }else{
    count[index]=count[index]-1;
    for(let i=0;i<order.length;i++){
      console.log(i);
      if(order[i].id==records[index].id){
        order[i].count=order[i].count-1;
        break;
      }
    }
  }
  this.setData({
    order:order,
    count:count,
  })
  
},
/**
 * 
 */
submit:function(){
  var that=this;
  wx.request({
    url: 'http://localhost:8080/tbl-order/recommendOrder',
    method: 'post',
    data:this.data.order,
    success (res){
      let list=res.data.data.list;
      console.log('list',res.data.data.list);
      wx.setStorage({
        data: list,
        key: 'list',
      })
      if(res.data.code==200){
          wx.navigateTo({
            url: '../refer1/refer1',     
          })
      }
    }
  })
},
  /**
   * 页面的初始数据
   */
  data: {
   
    count:[],
    current:1,
    size:-1,
    records:[],
    order:[],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showLoading({
      title: '数据加载中',
    })
    var that=this;
    wx.request({
      url: 'http://localhost:8080/tbl-component/queryAll/'+this.data.current+'/'+this.data.size,
      method: 'post',
      success (res){
        if(res.data.code==200){
          that.setData({
            records: res.data.data.records,
          });
          console.log(res.data);
          console.log(that.data.records);
        }
      },
      complete:function(){
        wx.hideLoading()
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    console.log("onReady");
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    console.log("onShow");
   
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
      
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    console.log("onrefresh");
    var that=this;
    wx.request({
      url: 'http://localhost:8080/tbl-component/queryAll/'+this.data.current+'/'+this.data.size,
      method: 'post',
      success (res){
        if(res.data.code==200){
          that.setData({
            records: res.data.data.records,
          });
          console.log("refresh",res.data);
          console.log("refresh",that.data.records);
        }
      }
    })
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})