// pages/compoent/conpoent.js
Page({

  findSupplier:function(e){
    let id=e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '../supplier/supplier?id='+id,
    })

  },
  /**
   * 页面的初始数据
   */
  data: {
    current:1,
    size:-1,
    records:[],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showLoading({
      title: '数据加载中',
    })
    var that=this;
    wx.request({
      url: 'http://localhost:8080/tbl-component/queryAll/'+this.data.current+'/'+this.data.size,
      method: 'post',
      success (res){
        if(res.data.code==200){
          that.setData({
            records: res.data.data.records,
          });
          console.log(res.data);
          console.log(that.data.records);
        }
      },
      complete:function(){
        wx.hideLoading()
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})