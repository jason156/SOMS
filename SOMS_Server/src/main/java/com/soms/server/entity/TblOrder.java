package com.soms.server.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 订购商表
 * </p>
 *
 * @author generator_gw
 * @since 2020-06-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="TblOrder对象", description="订购商表")
public class TblOrder implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "默认主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "订购商名称")
    private String name;

    @ApiModelProperty(value = "订购商电话")
    private String phone;

    @ApiModelProperty(value = "订购商地址")
    private String address;

    @ApiModelProperty(value = "联系人姓名")
    private String linkmanName;


}
