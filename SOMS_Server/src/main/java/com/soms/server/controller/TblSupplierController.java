package com.soms.server.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 供应商表 前端控制器
 * </p>
 *
 * @author generator_gw
 * @since 2020-06-11
 */
@RestController
@RequestMapping("/tbl-supplier")
public class TblSupplierController {

}

