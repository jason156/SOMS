package com.soms.server.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.soms.server.entity.TblSupplierComponent;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.soms.server.entity.vo.SupplierComponentVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 供应商零件关系表 Mapper 接口
 * </p>
 *
 * @author generator_gw
 * @since 2020-06-11
 */
@Repository
public interface TblSupplierComponentMapper extends BaseMapper<TblSupplierComponent> {

    // 查询供应商供应零件的信息
    Page<SupplierComponentVo> queryAllSupplierComponent(Page<SupplierComponentVo> page,
                                                        @Param("ew") QueryWrapper<SupplierComponentVo> queryWrapper);
    // 查询供应商供应的零件的种类个数
    int countComponent();

    // 根据id查询供应商供应零件的信息
    SupplierComponentVo querySupplierCompentById(Integer id);
}
