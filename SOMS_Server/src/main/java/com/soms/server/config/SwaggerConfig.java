package com.soms.server.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import static com.google.common.base.Predicates.not;
import static springfox.documentation.builders.RequestHandlerSelectors.withMethodAnnotation;

/**
 * <p>
 *     swagger配置类，访问swagger：http://localhost:8080/swagger-ui.html
 * </p>
 * @author guowei
 * @version 1.0
 * @since 2020/6/16
 */
@Configuration
public class SwaggerConfig {

    @Bean
    public Docket getDocket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(getApiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.soms.server.controller"))
                .apis(not(withMethodAnnotation(NotIncludeSwagger.class)))
                .build();
    }

    private ApiInfo getApiInfo() {
        return new ApiInfoBuilder().title("SOMS系统api文档").description("SOMS系统是由郭威和李少文开发的供应商订购管理系统").version("1.0.0")
                .contact(new Contact("郭威", "https://gitee.com/cxygw/SOMS.git", "1677488547@qq.com")).build();
    }
}
