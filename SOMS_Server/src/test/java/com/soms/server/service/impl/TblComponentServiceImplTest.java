package com.soms.server.service.impl;

import com.soms.server.entity.TblComponent;
import com.soms.server.service.TblComponentService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author GuoWei qq:1677488547
 * @version 1.0
 * @date 2020/6/15 23:38
 */
@Slf4j
@SpringBootTest
class TblComponentServiceImplTest {

    @Autowired
    private TblComponentService tblComponentService;
    @Test
    void queryAll() {
        List<TblComponent> tblComponents = tblComponentService.queryAll();
        log.info("零件信息："+tblComponents);
    }

    @Test
    void queryPage() {
    }

    @Test
    void queryByname() {
    }

    @Test
    void addByname() {
    }
}